package com.message.user.signup.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.beans.factory.annotation.Autowired;

import com.message.user.signup.exception.UserNotFoundException;
import com.message.user.signup.model.Signup;
import com.message.user.signup.util.SignupValidationUtil;

public class SignupValidationUtilTest {
	
	@Autowired
	private SignupValidationUtil signupValidationUtil = new SignupValidationUtil();
	
	
	@Test
	public void phoneNumberWthPrefixTest()  {
		
		String phoneNum="98 76 54321";
		String phone=signupValidationUtil.getConvertPhoneNumber(phoneNum);
		assertEquals("+33", phone.substring(0,3));
	}	
	
}
