package com.message.user.signup.service.impl;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.message.user.signup.exception.UserNotFoundException;
import com.message.user.signup.model.Signup;
import com.message.user.signup.util.SignupValidationUtil;

public class SignupMessagerPublisherTest {

	@Autowired
	private SignupMessagerPublisher classToTest = new SignupMessagerPublisher();	

	@Test(expected = UserNotFoundException.class)
	public void singupUserExpectionTest()  {
		
		Signup signup = new Signup();
		signup.setFullName(null);
		signup.setPhone("4334567");
		classToTest.publishMessage(signup);
	}	
	
	
	
}
