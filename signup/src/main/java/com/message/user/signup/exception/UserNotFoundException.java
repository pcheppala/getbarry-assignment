package com.message.user.signup.exception;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.*;


@ResponseStatus(HttpStatus.NOT_FOUND)
public class UserNotFoundException extends RuntimeException {

	public UserNotFoundException()
	{
		super();
	}

	public UserNotFoundException(String message) {
		// TODO Auto-generated constructor stub
	}
}
