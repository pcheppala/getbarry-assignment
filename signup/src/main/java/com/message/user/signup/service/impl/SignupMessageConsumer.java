package com.message.user.signup.service.impl;

import org.apache.log4j.Logger;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import com.message.user.signup.model.Signup;
import com.message.user.signup.service.IMessageConsumer;

@Component
public class SignupMessageConsumer implements IMessageConsumer{
	
	private static final Logger logger = Logger.getLogger(SignupMessageConsumer.class);

	@RabbitListener(queues = "${signups.queue.name}")
	public void recievedMessage(Signup signup) {
		
		logger.info("User " + signup.getFirstname() + signup.getLastName() + " with phone " + signup.getPhone() + "has just signed up!");
		
		}
}
