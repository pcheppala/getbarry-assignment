package com.message.user.signup.service.impl;

import org.apache.log4j.Logger;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.stereotype.Service;



@Service
public class SignupMessageListener implements MessageListener {
	
	private static final Logger logger = Logger.getLogger(SignupMessageListener.class);
	
		public void onMessage(Message message) {
			
			logger.info("Consuming Message from the listener- " + new String(message.getBody()));
		}

	}

