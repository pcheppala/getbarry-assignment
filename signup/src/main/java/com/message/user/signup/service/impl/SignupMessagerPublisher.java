package com.message.user.signup.service.impl;

import org.apache.log4j.Logger;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.message.user.signup.exception.InvalidPhoneNumberException;
import com.message.user.signup.exception.UserNotFoundException;
import com.message.user.signup.model.Response;
import com.message.user.signup.model.Signup;
import com.message.user.signup.service.IMessagePublisher;
import com.message.user.signup.util.SignupValidationUtil;

@Service
public class SignupMessagerPublisher implements IMessagePublisher{

	private static final Logger logger = Logger.getLogger(SignupMessagerPublisher.class);
	
	@Autowired
	private AmqpTemplate amqpTemplate;
	
	@Autowired
	private Response response;
	
	@Autowired
	private SignupValidationUtil signupValidationUtil;
	
	@Value("${signups.exchange.name}")
	private String exchange;
	
	@Value("${signups.routing.key}")
	private String routingkey;
	
	/*
	 * @sign
	 * Validate the signup input-params and publish message to queue
	 */
	public void publishMessage(Signup signup) {
		
		if(signup.getFullName()!= null && !(signup.getFullName().isEmpty())) {
			
			String[] nameDetails=signup.getFullName().split(" ");
			String fisrtName=nameDetails[0];
			String lastName=nameDetails[nameDetails.length-1];
			signup.setFirstname(fisrtName);
			signup.setLastName(lastName);
		}
		else {
			
			throw new UserNotFoundException();
		}
		if(signup.getPhone()!= null && !(signup.getPhone().isEmpty())) {
			
			String newPhone=signupValidationUtil.getConvertPhoneNumber(signup.getPhone());
			signup.setPhone(newPhone);
			response.setPhone(newPhone);
		}
		else {
			throw new InvalidPhoneNumberException();
		}
		
		
		amqpTemplate.convertAndSend(exchange, routingkey, signup);
		logger.info("signup message published with user : " + signup.getFullName() + signup.getPhone());
	    
	}
}