package com.message.user.signup.util;

import org.springframework.stereotype.Component;


@Component
public class SignupValidationUtil {
	
	private static final String PHONE_PREFIX="+33";	
/*
 * Convert the phone number
 */
	
 public String getConvertPhoneNumber(String phoneNum)
 {
	 
	 String newPhone = phoneNum.replaceAll("\\s", "");
		if(!newPhone.startsWith(PHONE_PREFIX))
			newPhone= PHONE_PREFIX+newPhone;
		return newPhone;
 }
	
}
