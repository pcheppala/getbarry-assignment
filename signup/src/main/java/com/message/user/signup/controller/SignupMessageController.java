package com.message.user.signup.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.message.user.signup.model.Response;
import com.message.user.signup.model.Signup;
import com.message.user.signup.service.IMessagePublisher;


@RestController
@RequestMapping(value = "/getBarry")
public class SignupMessageController {

	private static final Logger logger = Logger.getLogger(SignupMessageController.class);
	@Autowired
	IMessagePublisher messagePublisher;
	
	@Autowired
	Response response;

    @PostMapping(path = "/user-signup" , consumes = "application/json", produces = "application/json")
    public Response customerInformation(@RequestBody Signup signup) {
    	logger.info("signup user input-params :" + signup.getFullName() + signup.getPhone());
    	messagePublisher.publishMessage(signup);
    	response.setFirstname(signup.getFirstname());
    	response.setLastName(signup.getLastName());
    	response.setPhone(signup.getPhone());
    	return response;
    }
}


