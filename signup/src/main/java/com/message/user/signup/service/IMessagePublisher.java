package com.message.user.signup.service;

import com.message.user.signup.model.Signup;


public interface IMessagePublisher {
	
	public void publishMessage(Signup signup);
}