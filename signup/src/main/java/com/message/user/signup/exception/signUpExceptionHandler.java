package com.message.user.signup.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class signUpExceptionHandler {
   @ExceptionHandler(value = UserNotFoundException.class)
   public ResponseEntity<Object> exception(UserNotFoundException exception) {
      return new ResponseEntity<>("Signup User fullname not found", HttpStatus.NOT_FOUND);
   }
   
   @ExceptionHandler(value = InvalidPhoneNumberException.class)
   public ResponseEntity<Object> exception(InvalidPhoneNumberException exception) {
      return new ResponseEntity<>("Provide phone number is invalid", HttpStatus.NOT_FOUND);
   }
   
   
}